from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import json
import os
import requests
import smtplib

from dotenv import load_dotenv

load_dotenv()

CACHE_FILE = os.environ.get('CACHE_FILE')


def run():
    if not os.path.exists(CACHE_FILE):
        with open(CACHE_FILE, 'w') as f:
            json.dump([], f)

    with open(CACHE_FILE, 'r') as f:
        cache = json.load(f)

    r = requests.post('https://www.roomspot.nl/portal/object/frontend/getallobjects/format/json')
    if r.status_code != 200:
        print('Sending error email...')
        send_email('An error occurred', """Hi Nikster,
        
        Homebot encountered the following problem when trying to get all available rooms:
        
        Status code: {}
        Body: {}
        """.format(r.status_code, r.text))
        return

    rooms = [room for room in r.json()['result'] if room['isZelfstandig'] and not room['isExtraAanbod']]
    new_rooms = []
    for room in rooms:
        if room['id'] not in cache:
            new_rooms.append(room)

    if len(new_rooms) > 0:
        print('Sending new rooms email...')
        send_email('Nieuwe kamer beschikbaar!', """Hoi Nikster,
        
Geweldig nieuws: een of meerdere nieuwe zelfstandige kamers zijn beschikbaar! Bekijk ze hier:
        
{}
        
""".format('\n\n'.join(['https://www.roomspot.nl/aanbod/te-huur/details/{}'.format(room['urlKey']) for room in new_rooms])))

    with open(CACHE_FILE, 'w') as f:
        json.dump([room['id'] for room in rooms], f)


def send_email(subject, body):
    message = MIMEMultipart()
    message['From'] = os.environ.get('MAIL_FROM')
    message['To'] = os.environ.get('MAIL_TO')
    message['Subject'] = subject
    message.attach(MIMEText(body, 'plain'))
    text = message.as_string()

    session = smtplib.SMTP(os.environ.get('MAIL_HOST'), int(os.environ.get('MAIL_PORT')))
    session.starttls()
    session.login(os.environ.get('MAIL_USERNAME'), os.environ.get('MAIL_PASSWORD'))
    session.sendmail(os.environ.get('MAIL_FROM'), os.environ.get('MAIL_TO'), text)
    session.quit()


if __name__ == '__main__':
    run()
